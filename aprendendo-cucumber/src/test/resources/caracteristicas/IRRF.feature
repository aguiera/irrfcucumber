 # language: pt
  @IRRFTeste
  Funcionalidade: Calcular o imposto de renda retido da fonte (IRRF), para várias faixas e anos.
   
    Contexto: Cria cinco faixas (1 a 5) para dois anos a serem testados (2014 e 2017)
      Dado as faixas de irrf abaixo
        |  ano | faixa |  minimo |   maximo | deducao | aliquota |
        | 2017 |   1   |    0,00 |  1903,98 |    0,00 |    0,000 |
        | 2017 |   2   | 1903,99 |  2826,65 |  142,80 |    0,075 |
        | 2017 |   3   | 2826,66 |  3751,05 |  354,80 |    0,150 |
        | 2017 |   4   | 3751,06 |  4664,68 |  636,13 |    0,225 |
        | 2017 |   5   | 4664,69 | -1       |  869,36 |    0,275 |
 		| 2014 |   1   |    0,00 |  1787,77 |    0,00 |    0,000 |
 		| 2014 |   2   | 1787,78 |  2679,29 |  134,04 |    0,075 |
 		| 2014 |   3   | 2679,30 |  3572,43 |  335,03 |    0,150 |
 		| 2014 |   4   | 3572,44 |  4463,81 |  602,96 |    0,225 |
 		| 2014 |   5   | 4463,82 | -1       |  826,15 |    0,275 |
 		| 2008 |   1   |    0,00 |  1372,81 |    0,00 |    0,000 |
 		| 2008 |   2   | 1372,82 |  2743,25 |  205,92 |    0,150 |
 		| 2008 |   3   | 2743,26 |  -1      |  548,82 |    0,275 |
   
    Esquema do Cenario: Calcula o IRRF para o ano de 2017
      Dado o ano de <2017>
      Quando o usuário apresenta a <baseDeCalculo>
      Entao o <impostoaPagar> é calculado
      
      Exemplos:
      | baseDeCalculo | impostoaPagar |
      |       1500,00 |          0,00 |
      |       2755,23 |         63,84 |
      |       3365,32 |        150,00 |
      |       4289,93 |        329,10 |
      |      17523,47 |       3949,59 |

    Esquema do Cenario: Calcula o IRRF para o ano de 2014
      Dado o ano de <2014>
      Quando o usuário apresenta a <baseDeCalculo>
      Entao o <impostoaPagar> é calculado
      
      Exemplos:
      | baseDeCalculo | impostoaPagar |
      |       1500,00 |          0,00 |
      |       2650,32 |         64,73 |
      |       3365,32 |        169,77 |
      |       4289,93 |        362,27 |
      |      17523,47 |       3992,80 |
        
    Esquema do Cenario: Calcula o IRRF para o ano de 2008
      Dado o ano de <2008>
      Quando o usuário apresenta a <baseDeCalculo>
      Entao o <impostoaPagar> é calculado
      
      Exemplos:
      | baseDeCalculo | impostoaPagar |
      |       1285,23 |          0,00 |
      |       2542,98 |        175,53 |
      |      17584,69 |       4286,97 |
   