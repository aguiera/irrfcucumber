package cucumber.teste.passos;

import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import IRRF.FaixaIR;
import IRRF.IRRF;

import static org.junit.Assert.*;

public class IRRFTestePassos {
	   
    private List<FaixaIR> faixas;
    private List<FaixaIR> faixasSelecionadas;
    private double impostoRetido = 0;
    private double baseDeCalculo = 0;
    private IRRF calculadoraIRRF = new IRRF();
    
    
    
    @Dado("^as faixas de irrf abaixo$")
    public void recebimento_das_faixas_de_IRRF_por_ano(List<FaixaIR> faixas) throws Throwable {
        // Atribuição das faixas de IR
        this.faixas = faixas;
    }

    @Dado("^o ano de <(\\d+)>$")
    public void selecao_das_faixas_de_IRRF_para_um_determinado_ano(int ano) throws Throwable {
    	//Seleção do ano para cálculo
    	this.faixasSelecionadas = this.getFaixasIR(ano);
    }

    @Quando("^o usuário apresenta a (.+)$")
    public void guarda_da_base_de_calculo(double baseDeCalculo) throws Throwable {
    	this.baseDeCalculo = baseDeCalculo;
    	
    }

    @Entao("^o (.+) é calculado$")
    public void calcula_o_IRRF(double impostoaPagar) throws Throwable {
    	this.impostoRetido = calculadoraIRRF.calcularIRRF(this.baseDeCalculo, this.faixasSelecionadas);  
    	assertEquals("Erro no calculo do imposto", impostoaPagar, this.impostoRetido, 0.005);
    }
    
	private List<FaixaIR> getFaixasIR(int ano) {
		Stream<FaixaIR> stream =  this.faixas.stream().
				filter(faixa -> faixa.getAno() == ano);
		return stream.sorted((faixa1, faixa2) -> Long.compare(faixa1.getFaixa(),faixa2.getFaixa())).collect(Collectors.toList());
	}

}
